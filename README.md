# 🍃 *Smart Garden* (Documentation) 🌐

[![smart-garden](https://img.shields.io/static/v1?label=&message=Smart%20Garden&color=191919&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACkElEQVQ4T3VTTUhUURQ+Z37fjFpMmFmr3LZuU4sKyqKgRWZ/IES1MIICNRlTsgchpowWA0ltLFKohqJ9EGVB1KZNFAVqfxIWVI5vft57c+89nfucNw1iFw7vncv5vvOdn4vwnzNG73cRFEcFWMoGqyuJ+5+sFIrLL+/QfJMLdqoEVgsb+CYg90hA4byJZ2arMRWCDFGtA9YFAfnOEiwaPtCFRWAwE+UAyHEiUl4LhNyBJCYtTeQR3CM6qkCMMHADWzmr/9UqciChsBBSIm1I1R+Tcp6t+2RtzyTeJAqvArI5IKCzLRH44NwPVvSCCZ4TZO+b2P0z7YzMGlI2xaVw2+p6o5gmitZDyfaBZcnPJNi9HbDzFSBSdc037OGMIdShuJR0eHVfALn2CNfu+ASccSoE2NyOm0s+0CQzbqJZ0P54fuh2VMrjTAAHEhcRb9EnA6Gm6MsWUNxxDrdO6WCTrjcCqE38+8HEs9/13UTuypQhxDZNsG/NpSUFFsxVKbCbHPimXFCnFNhvTTj9kFvtlTGRH1gflPiFweG4kNBcD0GPYAGmywSWHtk4N+2PA9l+E9s92d4hwMn84F1DyCM6uyaghkCYCd5FfoNy3PLScA+GktjSU924sYXBRDwSTEdLpTYPXLaslTXQJAqthTdaQaAEhccK5lsdEK+JnK+GUjM8snVsuxlU52fmEUKUSH399THmLVKaXh5zIZuSkJuxYXGa38CJkJLAQOCF8czPWqN9JecQsWtLw+VMZZVNytSGwe3hHnQiuTFeWQ5U/8BCaL/I4JQjC0N7GlP5yipX12vS1Y0RwmFDUiuPCyvZlXwQAtW9N2F+XvExLX+V6eLw9hjBKBMEY0J1HEz0PV0eo/2/bjSnwFQoTgQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/smart-garden)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Get started

*Coming soon...*

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)