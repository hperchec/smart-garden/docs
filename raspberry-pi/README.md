# Raspberry Pi

## Prepare the image

### X - Update package manager

```bash
sudo apt-get update && sudo apt-get upgrade
```

### X - Install dependencies

- apache2:

  ```bash
  sudo apt-get install apache2
  ```

- php7.3, composer & required extensions:

  ```bash
  sudo apt-get install php7.3 composer php-xml php-mysql
  ```

- mariadb-server:

  ```bash
  sudo apt-get install mariadb-server
  ```

- git:

  ```bash
  sudo apt-get install git
  ```

- pigpio:

  ```bash
  sudo apt-get install pigpio
  ```

### X - Create **admin** user

- Create *admin* as sudoer (login: `admin`, password: `admin`):

  ```bash
  sudo adduser admin
  sudo adduser admin sudo
  sudo visudo /etc/sudoers.d/010_admin-nopasswd
  # insert content:
  admin ALL=(ALL) NOPASSWD: ALL
  # give rights to www-data
  sudo visudo
  # add to the end:
  # Allow www-data to execute command
  www-data ALL=(ALL) NOPASSWD:ALL
  ```

- Lock *pi* user:

  ```bash
  sudo usermod -L pi
  ```

  Reboot and login as `admin`

### X - MySQL configuration

- Disable *root* user & create *admin* user:

  ```bash
  sudo mysql
  # SQL script:
  CREATE USER 'admin'@'localhost' IDENTIFIED BY '';
  GRANT ALL ON *.* TO 'admin'@'localhost';
  FLUSH PRIVILEGES;
  ```

  Reboot system `sudo reboot`

### X - `smart-garden` directory

- Create directory:

  ```bash
  sudo mkdir /smart-garden
  ```

- Change owner to admin:

  ```bash
  sudo chown admin:admin /smart-garden
  ```

- Clone root repository

  ```bash
  git clone --recurse-submodules https://gitlab.com/hperchec/smart-garden/root.git /smart-garden
  ```

- SERVER

  ```bash
  sudo git clone https://gitlab.com/hperchec/smart-garden/app/server.git /smart-garden/server
  # Give permissions
  sudo chown -R admin:www-data /smart-garden/server
  sudo chmod -R 770 /smart-garden/server
  # Move into server
  cd /smart-garden/server
  # Copy/paste .env.prod.example to .env
  cp .env.prod.example /smart-garden/server/.env
  # Generate key
  php artisan key:generate
  # Generate passport oauth private key
  php artisan passport:keys
  # Link storage (symbolic link from /storage/app/public to /public/storage)
  php artisan storage:link
  # If you want to test the dev server:
  php artisan serve --host 0.0.0.0 --port 8000
  # TODO ? this is important for production
  sudo chown -R $USER:www-data storage
  sudo chown -R $USER:www-data bootstrap/cache
  ```

### X - Create `smart-garden` command

- Create a new file in `/usr/bin` and change chmod:

  ```bash
  sudo touch /usr/bin/smart-garden
  sudo chmod 755 /usr/bin/smart-garden
  ```

- Edit it (`sudo nano /usr/bin/smart-garden`) and put the following content:

  ```bash
  #!/bin/sh
  /smart-garden/cli/cli.sh $@
  ```

### X - Auto-start pigpiod service on boot

```bash
sudo systemctl enable pigpiod
```

### X - Init GPIO at startup

- Call the `smart-garden init` command at the end of the `/etc/rc.local` file (before `exit 0`):

  ```bash
  sudo sed -i '$ i\
  # -------------------------\
  # SMART GARDEN startup\
  # -------------------------\
  # Init GPIO ports\
  smart-garden init\
  # -------------------------\
  ' /etc/rc.local
  ```

### X - Database

- Create database

  ```bash
  /smart-garden/database/cmd/cli.sh create
  ```

- Migrate database

  ```bash
  /smart-garden/database/cmd/cli.sh migrate
  ```

- Seed database

  ```bash
  /smart-garden/database/cmd/cli.sh seed
  ```

### X - Apache configuration

- Disable default site:

  ```bash
  sudo a2dissite 000-default
  ```

- Copy apache2 VirtualHost configuration:

  ```bash
  sudo cp /smart-garden/apache2/conf/smart-garden.conf /etc/apache2/sites-available/smart-garden.conf
  ```

- Enable apache2 site: smart-garden

  ```bash
  sudo a2ensite smart-garden
  ```

- Enable apache2 rewrite module

  ```bash
  sudo a2enmod rewrite
  ```

- Restart apache2

  ```bash
  sudo systemctl reload apache2
  ```

### X - Extras

- Add some aliases

  ```bash
  echo "alias ll='ls -lArth'" >> ~/.bash_aliases
  ```

- Change default directory on login

  ```bash
  echo "cd /smart-garden" >> ~/.bashrc
  ```
